$(document).ready(function() {
    $('.item__slider-main-slide').lightSlider({
        item: 1,
        autoWidth: false,
        slideMove: 1,

        addClass: '',
        mode: "slide",

        speed: 300,
        auto: false,
        pauseOnHover: false,
        loop: false,
        slideEndAnimation: false,

        keyPress: false,
        controls: false,

        rtl:false,
        adaptiveHeight:false,

        vertical: true,
        verticalHeight:700,
        vThumbWidth: 120,

        thumbItem: 5,
        gallery: true,
        galleryMargin: 20,
        thumbMargin: 16,
        currentPagerPosition: 'middle',

        enableTouch:false,
        enableDrag:true,
        freeMove:true,
        swipeThreshold: 40,

        responsive : [],
    });

    const $select = $('.item__form-select');
    $select.css('width', '100%');
    $select.select2({
        language: 'ru',
        allowClear: !$select.prop('multiple') || !$select.attr('multiple'),
        minimumResultsForSearch: -1
    });

});


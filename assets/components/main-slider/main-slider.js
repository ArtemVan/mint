$(document).ready(function(){
    $('.main-slider').slick({
        slidesToShow: 1,
        arrows: true,
        nextArrow: '<svg class="main-slider__arrow main-slider__arrow--right"><use xlink:href="#arrow"></use></svg>',
        prevArrow: '<svg class="main-slider__arrow main-slider__arrow--left"><use xlink:href="#arrow"></use></svg>',
        dots: false
    });
});
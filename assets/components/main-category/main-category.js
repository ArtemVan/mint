$(document).ready(function(){
    $('.main-category__slider').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: true,
        nextArrow: '<svg class="main-category__arrow main-category__arrow--right"><use xlink:href="#arrow"></use></svg>',
        prevArrow: '<svg class="main-category__arrow main-category__arrow--left"><use xlink:href="#arrow"></use></svg>',
        dots: false
    });
});
$(document).ready(function(){
    $('.new-items__slider').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        arrows: true,
        nextArrow: '<svg class="new-items__slider-arrow new-items__slider-arrow--right"><use xlink:href="#arrow"></use></svg>',
        prevArrow: '<svg class="new-items__slider-arrow new-items__slider-arrow--left"><use xlink:href="#arrow"></use></svg>',
        dots: false,
        centerMode: false
    });
});
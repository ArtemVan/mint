# Mint

### Сборка ###

```bash
composer install
```

### Запустить сервер ###

```bash
composer run-script --timeout=0 server:run
```

### Запустить npm watch ###

```bash
composer run-script --timeout=0 npm:watch
```

### Сборка ассетов npm build ###

```bash
composer run-script npm:build
```
const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const WatchLiveReloadPlugin = require('webpack-watch-livereload-plugin');
const autoprefixer = require('autoprefixer');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const isProd = process.env.NODE_ENV === 'production';

const devPlugins = [
    new MiniCssExtractPlugin({
        filename: 'css/[name].css'
    }),
    new WatchLiveReloadPlugin({
        files: [
            './public/{fonts,css,js,images}/*.{css,png,svg,jp?eg,gif,js}'
        ]
    })
];

const prodPlugins = [
    new MiniCssExtractPlugin({
        filename: 'css/[name].css'
    }),
    new OptimizeCssAssetsPlugin()
];


const config = [{
    entry: {
        app: './assets/js/app.js',
        vendor: './assets/js/vendor.js'
    },
    output: {
        filename: 'js/[name].js',
        path: path.resolve(__dirname, './public')
    },
    optimization: {
        minimizer: [
            new TerserPlugin({
                test: /\.js(\?.*)?$/i,
                cache: true,
                parallel: true,
                sourceMap: !isProd,
                terserOptions: {
                    warnings: false,
                    parse: {},
                    compress: {},
                    ie8: false,
                    output: {
                        comments: false,
                    },
                },
            }),
        ]
    }
}, {
    entry: {
        styles: './assets/js/styles.js'
    },
    output: {
        filename: 'js/[name].js',
        path: path.resolve(__dirname, './public')
    },
    optimization: {
        minimizer: [
            new TerserPlugin({
                test: /\.js(\?.*)?$/i,
                cache: true,
                parallel: true,
                sourceMap: !isProd,
                terserOptions: {
                    warnings: false,
                    parse: {},
                    compress: {},
                    ie8: false,
                    output: {
                        comments: false,
                    },
                },
            }),
        ]
    },
    module: {
        rules: [
            {
                test: /\.(css|scss)$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader
                    },
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins: [
                                autoprefixer({
                                    browsers: [
                                        'ie >= 11',
                                        'last 10 version'
                                    ]
                                })
                            ]
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            precision: '6'
                        }
                    }
                ]
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                exclude: [/fonts/],
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '/images/[name].[ext]'
                        }
                    }
                ]
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/i,
                exclude: [/images/],
                use: {
                    loader: 'file-loader',
                    options: {
                        name: '/fonts/[name].[ext]'
                    }
                }
            }
        ]
    },
    plugins: isProd ? prodPlugins : devPlugins,
}];

module.exports = config;